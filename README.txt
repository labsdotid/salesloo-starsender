=== Salesloo Starsender ===
Contributors: fiqhid24
Tags: whatsapp, notification, message, salesloo
Requires at least: 5.0
Tested up to: 5.7.2
Stable tag: 0.0.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Starsender Whatsapp Notification for Salesloo Integration

== Description ==

Send whatsapp message notification on salesloo plugin with starsender api

== Installation ==

1. Upload `salesloo-starsender.zip` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==
= 1.0.1 =
* Fix some various bug

= 1.0.0 =
* First public release

== Upgrade Notice ==

<?php

namespace Salesloo_Starsender\Libraries;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('Starsender_Whatsapp')) {
    require_once(SALESLOO_STARSENDER_PATH . 'libraries/starsender/includes/whatsapp.php');
}

/**
 * Whatsapp classes
 */
class Whatsapp
{
    /**
     * Handle dynamic calls
     *
     * @param  string $name
     * @param  mixed $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        $wa = new \Starsender_Whatsapp();
        return $wa->$method(...$parameters);
    }

    /**
     * Handle dynamic static method calls.
     *
     * @param  string  $method
     * @param  mixed  $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }
}

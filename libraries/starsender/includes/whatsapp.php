<?php

/**
 * Starsender Whatsapp
 *
 * A simple class for send whatsapp message using starsender api
 *
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Starsender_Whatsapp
{

    private $_apikey = '';

    private $_to = '';

    private $_message = '';

    /**
     * set apikey
     *
     * @param  string $apikey
     * @return void
     */
    public function apikey($key)
    {
        $this->_apikey = $key;

        return $this;
    }


    /**
     * set recipients
     *
     * @param  string $to
     * @return void
     */
    public function to($phone)
    {
        $phone_to_check = str_replace('-', '', $phone);
        $phone_to_check = str_replace('+', '', $phone_to_check);
        $phone_to_check = str_replace(' ', '', $phone_to_check);

        $phone_to_check = preg_replace('/[^0-9]/', '', $phone_to_check);
        if (strlen($phone_to_check) < 9 || strlen($phone_to_check) > 14) return $this;

        $phone_to_check = preg_replace('/^620/', '62', $phone_to_check);
        $phone_to_check = preg_replace('/^0/', '62', $phone_to_check);

        $this->_to = $phone_to_check;
        return $this;
    }

    /**
     * set message
     *
     * @param  string $message
     * @return void
     */
    public function message($message, $args)
    {
        $message = $message;
        preg_match_all('@\[([^<>&/\[\]\x00-\x20=]++)@', $message, $matches);

        foreach ($matches[1] as $key => $tag) {
            if (isset($args[$tag])) {
                $message = str_replace('[' . $tag . ']', $args[$tag], $message);
            }
        }

        $this->_message = $message;

        return $this;
    }

    /**
     * curl
     *
     * @return mixed
     */
    public function send()
    {
        if (empty($this->_apikey) || empty($this->_to) || empty($this->_message)) return;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://starsender.online/api/sendText?message=' . rawurlencode($this->_message) . '&tujuan=' . rawurlencode($this->_to . '@s.whatsapp.net'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'apikey: ' . $this->_apikey
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}

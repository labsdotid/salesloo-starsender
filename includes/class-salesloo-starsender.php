<?php

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * @since      1.0.0
 * @package    Salesloo_Starsender
 * @subpackage Salesloo_Starsender/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Salesloo_Starsender
{
	/**
	 * instance
	 */
	public static $instance;

	/**
	 * Instance.
	 *
	 * Ensures only one instance of the plugin class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public static function run()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Define the core functionality of the plugin.
	 */
	public function __construct()
	{
		$this->load_dependencies();
		$this->load_notification_setting();
		$this->load_notification_classes();
	}

	/**
	 * load dependencies
	 *
	 * @return void
	 */
	private function load_dependencies()
	{
		require_once SALESLOO_STARSENDER_PATH . 'includes/class-salesloo-starsender-default-message.php';
		require_once SALESLOO_STARSENDER_PATH . 'includes/class-salesloo-starsender-whatsapp.php';
		require_once SALESLOO_STARSENDER_PATH . 'includes/class-salesloo-starsender-setting.php';
	}

	/**
	 * load notifictaion classes
	 *
	 * @return void
	 */
	private function load_notification_classes()
	{

		if (empty(salesloo_get_option('enable_starsender', 1))) return;

		add_filter('salesloo/notification/classes', function ($classes) {

			$classes[] = 'Salesloo_Starsender_Whatsapp';

			return $classes;
		});
	}

	private function load_notification_setting()
	{
		add_filter('salesloo/admin/settings/notification/sections', function ($sections) {

			$setting = new Salesloo_Starsender_Setting();

			$sections['startsender'] = [
				'label' => 'Starsender',
				'callback' => [$setting, 'setting']
			];

			return $sections;
		});
	}
}

<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

use Salesloo_Starsender_Default_Message as Message;

/**
 * The notification setting class.
 *
 * @since      1.0.0
 * @package    Salesloo_Starsender
 * @subpackage Salesloo_Starsender/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Salesloo_Starsender_Setting
{

    private $column = 'customer';

    private $tabbing = [];

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {

        $tabs = [
            'customer'  => __('Message for Customer', 'salesloo'),
            'affiliate' => __('Message for Affiliate', 'salesloo'),
            'admin'     => __('Message for Admin', 'salesloo')
        ];

        if (isset($_GET['column']) && isset($tabs[$_GET['column']])) {
            $this->column = sanitize_text_field($_GET['column']);
        }


        foreach ($tabs as $key => $label) {
            $url = admin_url('admin.php' . salesloo_current_query_args(['column' => $key]));
            $class = $key == $this->column ? 'current' : '';
            $this->tabbing[] = '<li><a class="' . $class . '" href="' . $url . '">' . $label . '</a></li>';
        }
    }

    /**
     * setting
     *
     * @return void
     */
    public function setting()
    {

        \salesloo_field_toggle([
            'label'           => __('Enable', 'salesloo'),
            'name'            => 'enable_starsender',
            'description'     => __('', 'salesloo'),
            'value'   => \salesloo_get_option('enable_starsender', 1),
        ]);

        \salesloo_field_text([
            'label'       => __('Apikey', 'salesloo'),
            'name'        => 'notification_starsender_apikey',
            'description' => 'Starsender api key <a target="blank" href="https://starsender.online/devices">Click here to get starsender api key</a>',
            'value'       => \salesloo_get_option('notification_starsender_apikey'),
        ]);

        \salesloo_field_textarea([
            'label'       => __('Admins Phone', 'salesloo'),
            'name'        => 'starsender_admins_phone',
            'description' => 'Phones admins for notification purpose, sparate by comma',
            'value'       => \salesloo_get_option('starsender_admins_phone')
        ]);

        echo '<div class="salesloo-field"><ul class="subsubsub">';
        echo implode(' | ', $this->tabbing);
        echo '</ul></div>';

        $this->for_customer();
        $this->for_affiliate();
        $this->for_admin();


        \salesloo_field_submit();
    }

    /**
     * message options for customer
     *
     * @return void
     */
    private function for_customer()
    {
        if ('customer' != $this->column) return;

        $shortcodes = [
            'site'           => __('Site name', 'salesloo'),
            'customer_name'  => __('Customer Name', 'salesloo'),
            'number'         => __('Invoice number', 'salesloo'),
            'payment_method' => __('Payment Method', 'salesloo'),
            'products'       => __('Purchased Products', 'salesloo'),
            'due_date'       => __('Due date Invoice', 'salesloo'),
            'payment_url'    => __('Payment link', 'salesloo')
        ];

        $place_order_shortcode = $shortcodes;

        \salesloo_field_textarea([
            'label'       => __('Place order', 'salesloo'),
            'name'        => 'starsender_message_place_order_customer',
            'description' => 'Message on place order event' . $this->print_shortcode($place_order_shortcode),
            'value'       => \salesloo_get_option('starsender_message_place_order_customer', Message::customer_place_order()),
            'style' => 'width:100%;height:200px;'
        ]);

        \salesloo_field_textarea([
            'label'       => __('Checking Payment Invoice', 'salesloo'),
            'name'        => 'starsender_message_invoice_checking_payment_customer',
            'description' => 'Message on checking payment event' . $this->print_shortcode($place_order_shortcode),
            'value'       => \salesloo_get_option('starsender_message_invoice_checking_payment_customer', Message::customer_invoice_checking_payment()),
            'style' => 'width:100%;height:200px;'
        ]);

        $invoice_completed_shortcode = $shortcodes;
        $invoice_completed_shortcode['access_url'] = __('Access product url');

        \salesloo_field_textarea([
            'label'       => __('Completed Invoice', 'salesloo'),
            'name'        => 'starsender_message_invoice_completed_customer',
            'description' => 'Message on completed invoice event' . $this->print_shortcode($invoice_completed_shortcode),
            'value'       => \salesloo_get_option('starsender_message_invoice_completed_customer', Message::customer_invoice_completed()),
            'style' => 'width:100%;height:200px;'
        ]);

        $order_expired_shortcode = $shortcodes;

        \salesloo_field_textarea([
            'label'       => __('Order Expired', 'salesloo'),
            'name'        => 'starsender_message_order_expired_customer',
            'description' => 'Message on order expired event' . $this->print_shortcode($order_expired_shortcode),
            'value'       => \salesloo_get_option('starsender_message_order_expired_customer', Message::customer_order_expired()),
            'style' => 'width:100%;height:200px;'
        ]);
    }

    /**
     * message options for affiliate
     *
     * @return void
     */
    private function for_affiliate()
    {
        if ('affiliate' != $this->column) return;

        $shortcodes = [
            'site'           => __('Site name', 'salesloo'),
            'affiliate_name' => __('Affiliate Name', 'salesloo'),
            'customer_name'  => __('Customer Name', 'salesloo'),
            'customer_email' => __('Customer Email', 'salesloo'),
            'number'         => __('Invoice number', 'salesloo'),
            'product'        => __('Purchased Product', 'salesloo'),
            'commission'     => __('Commission', 'salesloo'),
        ];

        $place_order_shortcode = $shortcodes;

        \salesloo_field_textarea([
            'label'       => __('Place order', 'salesloo'),
            'name'        => 'starsender_message_place_order_affiliate',
            'description' => 'Message on place order event' . $this->print_shortcode($place_order_shortcode),
            'value'       => \salesloo_get_option('starsender_message_place_order_affiliate', Message::affiliate_place_order()),
            'style' => 'width:100%;height:200px;'
        ]);

        $invoice_completed_shortcode = $shortcodes;

        \salesloo_field_textarea([
            'label'       => __('Completed Invoice', 'salesloo'),
            'name'        => 'starsender_message_invoice_completed_affiliate',
            'description' => 'Message on completed invoice event' . $this->print_shortcode($invoice_completed_shortcode),
            'value'       => \salesloo_get_option('starsender_message_invoice_completed_affiliate', Message::affiliate_invoice_completed()),
            'style' => 'width:100%;height:200px;'
        ]);

        $commission_paid_shortcode = [
            'amount'           => __('Amount of commission', 'salesloo'),
            'method'           => __('Payout method', 'salesloo'),
            'note'             => __('Payout Note', 'salesloo'),
            'date'             => __('Date of payout', 'salesloo'),
            'affiliate_name'   => __('Affiliate Name')
        ];

        \salesloo_field_textarea([
            'label'       => __('Commission Paid', 'salesloo'),
            'name'        => 'starsender_message_commission_paid_affiliate',
            'description' => 'Message on order expired event' . $this->print_shortcode($commission_paid_shortcode),
            'value'       => \salesloo_get_option('starsender_message_commission_paid_affiliate', Message::affiliate_commission_paid()),
            'style' => 'width:100%;height:200px;'
        ]);
    }

    /**
     * message options for customer
     *
     * @return void
     */
    private function for_admin()
    {
        if ('admin' != $this->column) return;

        $shortcodes = [
            'site'           => __('Site name', 'salesloo'),
            'customer_name'  => __('Customer Name', 'salesloo'),
            'customer_email' => __('Customer Email', 'salesloo'),
            'number'         => __('Invoice number', 'salesloo'),
            'payment_method' => __('Payment Method', 'salesloo'),
            'products'       => __('Purchased Products', 'salesloo'),
            'due_date'       => __('Due date Invoice', 'salesloo'),
            'invoice_url'    => __('Payment link', 'salesloo')
        ];

        $place_order_shortcode = $shortcodes;

        \salesloo_field_textarea([
            'label'       => __('Place order', 'salesloo'),
            'name'        => 'starsender_message_place_order_admin',
            'description' => 'Message on place order event' . $this->print_shortcode($place_order_shortcode),
            'value'       => \salesloo_get_option('starsender_message_place_order_admin', Message::admin_place_order()),
            'style' => 'width:100%;height:200px;'
        ]);

        \salesloo_field_textarea([
            'label'       => __('Checking Payment Invoice', 'salesloo'),
            'name'        => 'starsender_message_invoice_checking_payment_admin',
            'description' => 'Message on checking payment event' . $this->print_shortcode($place_order_shortcode),
            'value'       => \salesloo_get_option('starsender_message_invoice_checking_payment_admin', Message::admin_invoice_checking_payment()),
            'style' => 'width:100%;height:200px;'
        ]);

        $invoice_completed_shortcode = $shortcodes;

        \salesloo_field_textarea([
            'label'       => __('Completed Invoice', 'salesloo'),
            'name'        => 'starsender_message_invoice_completed_admin',
            'description' => 'Message on completed invoice event' . $this->print_shortcode($invoice_completed_shortcode),
            'value'       => \salesloo_get_option('starsender_message_invoice_completed_admin', Message::customer_invoice_completed()),
            'style' => 'width:100%;height:200px;'
        ]);
    }


    /**
     * print shortcode
     *
     * @param  mixed $tags
     * @return void
     */
    private function print_shortcode($tags)
    {
        ob_start();
        echo '<br/>';
        echo 'print shortcode<br/>';
        foreach ($tags as $tag => $note) {
            echo '<strong>[' . $tag . ']</strong> => ' . $note . '<br/>';
        }
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }
}

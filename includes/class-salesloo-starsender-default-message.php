<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * The notification plugin class.
 *
 * @since      1.0.0
 * @package    Salesloo_Starsender
 * @subpackage Salesloo_Starsender/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Salesloo_Starsender_Default_Message
{
    /**
     * customer_place_order default message
     *
     * @return void
     */
    public static function customer_place_order()
    {
        $message = 'Halo [customer_name] terima kasih telah melakukan pembelian di [site],

Berikut pesananmu ya

Nomor Invoice :
*[number]*

Produk :
*[products]*

Total Pembayaran :
*[total]*

Methode Pembayaran :
*[payment_method]*

Klik di sini untuk informasi pembayaran
[payment_url].


Harap segera lakukan pembayaran sebelum tanggal [due_date] agar bisa kami proses ya';

        return $message;
    }

    /**
     * customer_invoice_checking_payment default message
     *
     * @return void
     */
    public static function customer_invoice_checking_payment()
    {
        $message = 'Terima kasih [customer_name] atas pembayaran yang Anda lakukan,

Detail Invoice

Nomor Invoice :
*[number]*

Total Pembayaran :
*[total]*

Methode Pembayaran :
*[payment_method]*


Sesegera mungkin kami akan mengaktifkan pembelian Anda setelah pembayaran Anda terverifikasi';

        return $message;
    }

    /**
     * customer_invoice_completed default message
     *
     * @return void
     */
    public static function customer_invoice_completed()
    {
        $message = 'Halo [customer_name] terima kasih atas pembelian Anda,

Kami telah memverifikasi pembayaran dan telah mengaktifkan pembelian Anda.

Nomor Invoice :
*[number]*

Produk :
*[products]*

Akses product Anda di sini :
*[access_url]*';

        return $message;
    }

    /**
     * customer_order_expired default message
     *
     * @return void
     */
    public static function customer_order_expired()
    {
        $message = 'Halo [customer_name] pembelian Anda atas salah satu produk di [site] telah kadaluarsa,

Silahkan lakukan perpanjangan untuk tetap dapat menikmati akses produk tersebut,
Berikut detailnya.

Nomor Invoice :
*[number]*

Produk :
*[products]*

Total Pembayaran :
*[total]*

Methode Pembayaran :
*[payment_method]*

Klik di sini untuk informasi pembayaran
[payment_url].


Harap segera lakukan pembayaran sebelum tanggal [due_date] agar bisa kami proses ya';

        return $message;
    }

    /**
     * affiliate_place_order default message
     *
     * @return void
     */
    public static function affiliate_place_order()
    {
        $message = 'Terima kasih [affiliate_name] ada pembeli baru di [site] atas referensi kamu,

Berikut detailnya

Nomor Invoice :
*[number]*

Pembeli :
*[customer_name] ([customer_email])*

Produk :
*[product]*

Potensi komisi kamu jika pembeli meenyelesaikan pembayarannya :
*[commission]*


Terima kasih banyak';

        return $message;
    }

    /**
     * affiliate_invoice_completed default message
     *
     * @return void
     */
    public static function affiliate_invoice_completed()
    {
        $message = 'Hi [affiliate_name] selamat, salah satu pembeli di [site] yang kamu referensikan telah menyelesaikan pembayaran, kamu mednapatkan komisi atas kerja keras kamu.

Berikut detailnya

Nomor Invoice :
*[number]*

Produk :
*[product]*

Komisi :
*[commission]*

Silahkan cek detail komisi kamu di member area, Terima Kasih';

        return $message;
    }

    /**
     * affiliate_commission_paid default message
     *
     * @return void
     */
    public static function affiliate_commission_paid()
    {
        $message = 'Hi [affiliate_name] kami telah melakukan payout komisi kamu.

Berikut detailnya

Jumlah payout :
*[amount]*

Komisi sampai Tanggal :
*[date]*

Catatan :
*[note]*

Silahkan cek detail komisi kamu di member area, Terima Kasih';

        return $message;
    }

    /**
     * admin_place_order default message
     *
     * @return void
     */
    public static function admin_place_order()
    {
        $message = 'Halo mimin ada pembelian baru di [site],

Berikut detail pesanannya

Nomor Invoice :
*[number]*

Pembeli :
*[customer_name] ([customer_email])*

Produk :
*[products]*

Total Pembayaran :
*[total]*

Methode Pembayaran :
*[payment_method]*

Klik di sini untuk check invoice
[invoice_url].';

        return $message;
    }

    /**
     * admin_invoice_checking_payment default message
     *
     * @return void
     */
    public static function admin_invoice_checking_payment()
    {
        $message = 'Halo mimin ada pembeli yang melakukan konfirmasi pembayaran di [site],

Berikut detail pesanannya

Nomor Invoice :
*[number]*

Pembeli :
*[customer_name] ([customer_email])*

Produk :
*[products]*

Total Pembayaran :
*[total]*

Methode Pembayaran :
*[payment_method]*

Klik di sini untuk check invoice
[invoice_url].';

        return $message;
    }

    /**
     * admin_invoice_completed default message
     *
     * @return void
     */
    public static function admin_invoice_completed()
    {
        $message = 'Sukses min, salah satu pembelian di [site] telah di aktifkan,

Berikut detail pesanannya

Nomor Invoice :
*[number]*

Pembeli :
*[customer_name] ([customer_email])*

Produk :
*[products]*

Total Pembayaran :
*[total]*

Methode Pembayaran :
*[payment_method]*

Klik di sini untuk check invoice
[invoice_url].';

        return $message;
    }
}

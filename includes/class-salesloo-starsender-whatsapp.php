<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

use \Salesloo\Abstracts\Notification;
use \Salesloo_Starsender\Libraries\Whatsapp;
use \Salesloo_Starsender_Default_Message as Message;
use \Salesloo\Models\Commission;

/**
 * The Send Whatsapp class.
 *
 * @since      1.0.0
 * @package    Salesloo_Starsender
 * @subpackage Salesloo_Starsender/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Salesloo_Starsender_Whatsapp extends Notification
{
    private $wa = null;

    private $admin_phones = '';

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->id = 'starsender';
        $this->title = 'Starsender';

        $this->admin_phones = explode(',', \salesloo_get_option('starsender_admins_phone'));

        $apikey = salesloo_get_option('notification_starsender_apikey');
        $this->wa = Whatsapp::apikey($apikey);
    }

    /**
     * on_place_order event
     * 
     * send whatsapp notification on place order event
     *
     * @param  array $args
     * @return void
     */
    public function on_place_order($args)
    {

        $invoice_id = isset($args['invoice_id']) ? intval($args['invoice_id']) : 0;
        $invoice  = salesloo_get_invoice($invoice_id);

        if (!$invoice) return;

        $summary  = $invoice->summary;
        $orders   = $invoice->orders();
        $products = $summary['products'];

        unset($summary['products']);

        $customer           = get_userdata($invoice->user_id);
        $encoded_invoice_id = salesloo_encrypt($invoice->ID);

        $product_titles = [];
        foreach ($products as $p) {
            $product_titles[] = $p['label'];
        }

        $total = $summary['total']['converted']['print'] ? $summary['total']['converted']['print'] : $summary['total']['print'];

        $variables = [
            'site'           => get_bloginfo('name'),
            'number'         => $invoice->number,
            'payment_method' => salesloo_get_payment_methods($invoice->payment_method)->get_title(),
            'products'       => implode(', ', $product_titles),
            'total'          => $total,
            'due_date'       => date('Y-m-d H:i', strtotime($invoice->due_date_at)),
            'customer_name'  => salesloo_user_get_name($customer),
            'customer_email'  => $customer->user_email,
            'payment_url'    => salesloo_url_payment($encoded_invoice_id),
        ];

        /**
         * send message to customer if have phone number
         */
        $customer_message = salesloo_get_option('starsender_message_place_order_customer', Message::customer_place_order());
        $this->send_to_customer($customer->ID, $customer_message, $variables);

        /**
         * send message to affiliate
         */
        $affiliate_message = salesloo_get_option('starsender_message_place_order_affiliate', Message::affiliate_place_order());
        $this->send_to_affiliate($invoice_id, $orders, $affiliate_message, $variables);

        /**
         * send message to admins
         */
        $admin_message = salesloo_get_option('starsender_message_place_order_admin', Message::admin_place_order());
        $this->send_to_admins($admin_message, $variables);

        return;
    }

    /**
     * on_invoice_checking_payment event
     * 
     * send whatsapp message notification on invoice checking payment event
     *
     * @param  array $args
     * @return void
     */
    public function on_invoice_checking_payment($args)
    {
        $invoice_id = isset($args['invoice_id']) ? intval($args['invoice_id']) : 0;
        $invoice = salesloo_get_invoice($invoice_id);

        if (!$invoice) return;

        $summary  = $invoice->summary;
        $products = $summary['products'];

        unset($summary['products']);

        $customer           = get_userdata($invoice->user_id);
        $encoded_invoice_id = salesloo_encrypt($invoice->ID);

        $product_titles = [];
        foreach ($products as $p) {
            $product_titles[] = $p['label'];
        }

        $total = $summary['total']['converted']['print'] ? $summary['total']['converted']['print'] : $summary['total']['print'];

        $variables = [
            'site'           => get_bloginfo('name'),
            'number'         => $invoice->number,
            'payment_method' => salesloo_get_payment_methods($invoice->payment_method)->get_title(),
            'products'       => implode(', ', $product_titles),
            'total'          => $total,
            'due_date'       => date('Y-m-d H:i', strtotime($invoice->due_date_at)),
            'customer_name'  => salesloo_user_get_name($customer),
            'customer_email'  => $customer->user_email,
            'payment_url'    => salesloo_url_payment($encoded_invoice_id),
        ];

        /**
         * send message to customer if have phone number
         */
        $customer_message = salesloo_get_option('starsender_message_invoice_checking_payment_customer', Message::customer_invoice_checking_payment());
        $this->send_to_customer($customer->ID, $customer_message, $variables);

        /**
         * send message to admins
         */
        $admin_message = salesloo_get_option('starsender_message_invoice_checking_payment_admin', Message::admin_invoice_checking_payment());
        $this->send_to_admins($admin_message, $variables);

        return;
    }

    /**
     * on_invoice_completed event
     *
     * send notifcation whatsapp message on invoice completed event
     * 
     * @param  array $args
     * @return void
     */
    public function on_invoice_completed($args)
    {
        $invoice_id = isset($args['invoice_id']) ? intval($args['invoice_id']) : 0;
        $invoice = salesloo_get_invoice($invoice_id);

        if (!$invoice) return;

        $summary = $invoice->summary;
        $orders = $invoice->orders();
        $products = $summary['products'];

        unset($summary['products']);

        $customer           = get_userdata($invoice->user_id);
        $encoded_invoice_id = salesloo_encrypt($invoice->ID);

        $product_titles = [];
        foreach ($products as $p) {
            $product_titles[] = $p['label'];
        }

        $total = isset($summary['total']['converted']['print']) ? $summary['total']['converted']['print'] : $summary['total']['print'];

        $variables = [
            'site'           => get_bloginfo('name'),
            'number'         => $invoice->number,
            'payment_method' => salesloo_get_payment_methods($invoice->payment_method)->get_title(),
            'products'       => implode(', ', $product_titles),
            'total'          => $total,
            'due_date'       => date('Y-m-d H:i', strtotime($invoice->due_date_at)),
            'customer_name'  => salesloo_user_get_name($customer),
            'payment_url'    => salesloo_url_payment($encoded_invoice_id),
            'access_url'    => salesloo_url_access(),
        ];

        /**
         * send message to customer if have phone number
         */
        $customer_message = salesloo_get_option('starsender_message_invoice_completed_customer', Message::customer_invoice_completed());
        $this->send_to_customer($customer->ID, $customer_message, $variables);

        /**
         * send message to affiliate
         */
        $affiliate_message = salesloo_get_option('starsender_message_invoice_completed_affiliate', Message::affiliate_invoice_completed());
        $this->send_to_affiliate($invoice_id, $orders, $affiliate_message, $variables);

        /**
         * send message to admins
         */
        $admin_message = salesloo_get_option('starsender_message_invoice_completed_admin', Message::admin_invoice_completed());
        $this->send_to_admins($admin_message, $variables);

        return;
    }

    /**
     * on_commission_paid event
     * 
     * send notification message to affiliate on commission paid
     *
     * @param  mixed $args
     * @return void
     */
    public function on_commission_paid($args)
    {

        $withdrawal_id = isset($args['withdrawal_id']) ? intval($args['withdrawal_id']) : 0;
        $withdrawal = salesloo_get_commission_withdrawal($withdrawal_id);
        if (!$withdrawal) return;

        $affiliate = get_userdata($withdrawal->user_id);

        /**
         * skip if invalid affiliate id
         */
        if (!$affiliate) return;

        $variables = [
            'amount'           => salesloo_convert_money($withdrawal->amount),
            'method'           => $withdrawal->method,
            'note'             => $withdrawal->note,
            'proof_of_payment' => $withdrawal->proof_of_payment,
            'date'             => date('Y-m-d H:i', strtotime($withdrawal->created_at)),
            'affiliate_name'   => salesloo_user_get_name($affiliate)
        ];

        $message = salesloo_get_option('starsender_message_commission_paid_affiliate', Message::affiliate_commission_paid());

        $to = get_user_meta($affiliate->ID, 'phone', true);
        if ($to) {
            $this->wa->to($to)->message($message, $variables)->send();
        }

        return;
    }

    /**
     * on_order_expired
     * 
     * send notification message on order expired event
     *
     * @param  mixed $args
     * @return void
     */
    public function on_order_expired($args)
    {
        $invoice_id = isset($args['invoice_id']) ? intval($args['invoice_id']) : 0;
        $invoice = salesloo_get_invoice($invoice_id);

        if (!$invoice) return;

        $summary = $invoice->summary;
        $products = $summary['products'];
        unset($summary['products']);
        $customer           = get_userdata($invoice->user_id);
        $encoded_invoice_id = salesloo_encrypt($invoice->ID);

        $product_titles = [];
        foreach ($products as $p) {
            $product_titles[] = $p['label'];
        }

        $total = $summary['total']['converted']['print'] ? $summary['total']['converted']['print'] : $summary['total']['print'];

        $variables = [
            'site'           => get_bloginfo('name'),
            'number'         => $invoice->number,
            'payment_method' => salesloo_get_payment_methods($invoice->payment_method)->get_title(),
            'products'       => implode(', ', $product_titles),
            'total'          => $total,
            'due_date'       => date('Y-m-d H:i', strtotime($invoice->due_date_at)),
            'customer_name'  => salesloo_user_get_name($customer),
            'payment_url'    => salesloo_url_payment($encoded_invoice_id),
        ];

        /**
         * send message to customer if have phone number
         */
        $message = salesloo_get_option('starsender_message_order_expired_customer', Message::customer_order_expired());
        $this->send_to_customer($customer->ID, $message, $variables);

        return;
    }

    public function on_register($args)
    {
        return;
    }

    /**
     * send notification message to customer
     *
     * @param  int $customer_id
     * @param  string $message
     * @param  array $variables
     * @return void
     */
    private function send_to_customer($customer_id, $message, $variables)
    {
        $to = get_user_meta($customer_id, 'phone', true);
        if ($to) {
            $this->wa->to($to)->message($message, $variables)->send();
        }
    }

    /**
     * send notification message to admins
     *
     * @param  string $message
     * @param  array $variables
     * @return void
     */
    private function send_to_admins($message, $variables)
    {
        foreach ((array)$this->admin_phones as $phone) {
            $this->wa->to($phone)->message($message, $variables)->send();
        }
    }

    /**
     * send notification messaage to affiliates
     *
     * @param  int $invoice_id
     * @param  array $orders
     * @param  string $message
     * @param  array $variables
     * @return void
     */
    private function send_to_affiliate($invoice_id, $orders, $message, $variables)
    {
        foreach ($orders as $order) {
            $affiliate      = get_userdata($order->affiliate_id);

            /**
             * skip if no valid affiliate_id
             */
            if (!$affiliate) continue;

            $commission = Commission::where('user_id', $affiliate->ID, '=')
                ->andWhere('invoice_id', $invoice_id, '=')
                ->first();

            /**
             * skip if no commissions
             */
            if ($commission->ID <= 0) continue;

            $product = salesloo_get_product($order->product_id);

            $add_variables = [
                'affiliate_name' => salesloo_user_get_name($affiliate),
                'product'        => $product->title,
                'commission'     => salesloo_convert_money($commission->amount),
            ];

            /**
             * merge args
             */
            $args = wp_parse_args($add_variables, $variables);

            /**
             * get affiliate phone number
             */
            $to = get_user_meta($affiliate->ID, 'phone', true);

            /**
             * send
             */
            if ($to) {
                $this->wa->to($to)->message($message, $args)->send();
            }
        }
    }
}

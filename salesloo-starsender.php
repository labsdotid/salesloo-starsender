<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.fiqhidayat.com
 * @since             1.0.0
 * @package           Salesloo_Starsender
 *
 * @wordpress-plugin
 * Plugin Name:       Salesloo Starsender
 * Plugin URI:        https://labs.id
 * Description:       Starsender Whatsapp Notification for Salesloo Integration
 * Version:           1.0.2
 * Author:            Labs ID Teams
 * Author URI:        https://www.labs.id
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       salesloo-starsender
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('SALESLOO_STARSENDER_VERSION', '1.0.2');
define('SALESLOO_STARSENDER_URL', plugin_dir_url(__FILE__));
define('SALESLOO_STARSENDER_PATH', plugin_dir_path(__FILE__));
define('SALESLOO_STARSENDER_ROOT', __FILE__);

require 'update-checker/plugin-update-checker.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-salesloo-starsender-activator.php
 */
function activate_salesloo_starsender()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-salesloo-starsender-activator.php';
	Salesloo_Starsender_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-salesloo-starsender-deactivator.php
 */
function deactivate_salesloo_starsender()
{
	require_once SALESLOO_STARSENDER_PATH . 'includes/class-salesloo-starsender-deactivator.php';
	Salesloo_Starsender_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_salesloo_starsender');
register_deactivation_hook(__FILE__, 'deactivate_salesloo_starsender');


/**
 * load plugin text domain for plugin translation
 *
 * @return void
 */
function salesloo_starsender_load_plugin_textdomain()
{
	load_plugin_textdomain(
		'salesloo-starsender',
		false,
		dirname(plugin_basename(__FILE__)) . '/languages'
	);

	if (did_action('salesloo/loaded')) {

		/**
		 * The core plugin class
		 */
		require SALESLOO_STARSENDER_PATH . 'libraries/starsender/whatsapp.php';
		require SALESLOO_STARSENDER_PATH . 'includes/class-salesloo-starsender.php';

		Salesloo_Starsender::run();
	}
}
add_action('plugins_loaded', 'salesloo_starsender_load_plugin_textdomain');
